module.exports = {
  verbose: true,
  collectCoverage: true,
  collectCoverageFrom:[
  "**/src/**.js"],
  coverageReporters: ["text", "text-summary", "json", "lcov", "clover"],
  "coverageThreshold": {
      "global": {
        "branches": 80,
        "functions": 80,
        "lines": 80,
        "statements": -10
      }
    }
};
