const sum = require('./src/sum');

test('adds 1 + 2 to equal 3', () => {
 expect(sum(1, 2)).toBe(3);
});
test('adds 2 + 2 to equal 4', () => {
 expect(sum(2, 2)).toBe(4);
});
//test('1 to equal 1 3', () => {
// expect(1).toBe(1);
//});
